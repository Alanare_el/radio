#include <SPI.h>
#include "RF24.h"

RF24 radio(9,10);

struct myData{ // То что отправляется с микроконтроллера
  int data0;
  int data1;
  int data2;
  int data3;
  int data4;
  int data5;
  byte counter;
};

struct reciever{
  byte gotByte;
};

myData set_random_data(myData a){
  a.data0 = 100 + rand() % 100;
  a.data1 = 100 + rand() % 100;
  a.data2 = 100 + rand() % 100;
  a.data3 = 100 + rand() % 100;
  a.data4 = 100 + rand() % 100;
  a.data5 = 100 + rand() % 100;
  return a;
}

const uint64_t pipes[2] = { 0xABCDABCD71LL, 0x544d52687CLL };                                                           

byte counter = 1;

void setup(){

  Serial.begin(115200);

  radio.begin();
  radio.setPayloadSize(sizeof(myData));
  radio.setAutoAck(1);                    
  radio.enableAckPayload();               
  radio.setRetries(0,15);                 
  radio.openWritingPipe(pipes[0]);        
  radio.openReadingPipe(1,pipes[1]);
  radio.startListening();                 
  radio.printDetails();                   
}


void loop(void) {

  myData recieve;
  reciever resp;
  byte pipeNo;
    
  if(radio.available()){
    while( radio.available(&pipeNo)){
      radio.read( &recieve, sizeof(recieve) );
      Serial.println(recieve.counter);
      resp.gotByte = recieve.counter;
      radio.writeAckPayload(pipeNo, &resp, sizeof(resp) ); 
    }
  }
}