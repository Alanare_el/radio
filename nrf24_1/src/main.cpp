#include <SPI.h>
#include "RF24.h"
#include "printf.h"

RF24 radio(9,10);

struct myData{ // Пакет отправляемый микроконтроллером
  int data0;
  int data1;
  int data2;
  int data3;
  int data4;
  int data5;
  byte counter;
};

struct reciever{  // Пакет отправляемый пультом
  byte gotByte;
};

myData set_random_data(myData a){ // Просто рандомный метод, писал для себя(тупо тестил)
  a.data0 = 100 + rand() % 100;
  a.data1 = 100 + rand() % 100;
  a.data2 = 100 + rand() % 100;
  a.data3 = 100 + rand() % 100;
  a.data4 = 100 + rand() % 100;
  a.data5 = 100 + rand() % 100;
  return a;
}

const uint64_t pipes[2] = { 0xABCDABCD71LL, 0x544d52687CLL }; // Идентификаторы труб      

byte counter = 1; // Счетчик пакетов

void setup(){

  Serial.begin(115200);
  printf_begin();

  radio.begin();
  radio.setPayloadSize(sizeof(myData));
  radio.setAutoAck(1);                    
  radio.enableAckPayload();               
  radio.setRetries(0,15);                 
  radio.openWritingPipe(pipes[1]);        
  radio.openReadingPipe(1,pipes[0]);
  radio.startListening();                 
  radio.printDetails();                   
}


void loop(void) {

  myData data = set_random_data(data);
  data.counter = counter;
  Serial.println(data.data0);

  radio.stopListening();                                  
        
  printf("Сейчас отправлено %d ",counter);
  reciever req; 
  unsigned long time = micros();                          
                                                            
  if (!radio.write( &data, sizeof(data) )){
    Serial.println(F("Ошибка."));     
  }else{
    if(!radio.available()){ 
      Serial.println(F("Вернулась пустота.")); 
    }else{
        
      while(radio.available() ){
        unsigned long tim = micros();
        radio.read( &req, sizeof(req) );
        printf("Получили ответ %d, задержка туда-обратно: %lu мкс\n\r",req.gotByte,tim-time);
        counter++;
      }
    }

  }
  delay(1000);
}